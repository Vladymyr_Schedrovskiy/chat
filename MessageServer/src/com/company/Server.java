package com.company;

import java.io.*;
import java.net.*;
import java.util.ArrayList;

public class Server
{
    private static Boolean _started = false;
    public static void main(String[] args) throws IOException {
        System.out.println("Старт сервера");
        // поток для чтения данных
        BufferedReader in = null;
        // поток для отправки данных
        PrintWriter    out= null;
        // серверный сокет
        ServerSocket server = null;
        // сокет для обслуживания клиента
        Socket       client = null;

        ArrayList<Socket> clients = new ArrayList<Socket>();

// создаем серверный сокет
        try {
            server = new ServerSocket(1234);
        } catch (IOException e) {
            System.out.println("Ошибка связывания с портом 1234");
            System.exit(-1);
        }

        _started = true;
        while (_started)
        {

            try {
                System.out.print("Ждем соединения");
                client= server.accept();
                System.out.println("Клиент подключился");
            } catch (IOException e) {
                System.out.println("Не могу установить соединение");
                System.exit(-1);
            }
            ClientProcessor clientProcessor = new ClientProcessor(client, clients);
            clients.add(client);
            Thread t = new Thread(clientProcessor);
            t.start();
        }


        out.close();
        in.close();
        client.close();
        server.close();
    }


}

class ClientProcessor  implements Runnable {

    ArrayList<Socket> _clients;

    BufferedReader in = null;

    PrintWriter    out= null;

    private Socket _client;

    public ClientProcessor(Socket _client, ArrayList<Socket> clients) {
        this._client = _client;
        this._clients = clients;
    }

    @Override
    public void run() {

        try {
            process();
        }
        catch (SocketException e)
        {
            stop();
        }
        catch (IOException e) {
            e.printStackTrace();
        }
    }
    void stop() {
        try {
            in.close();
            if(out != null)
            {
                out.close();
            }
            _client.close();
            _clients.remove(_client);
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
    }

    private void process() throws IOException
    {
        in  = new BufferedReader(
                new InputStreamReader(_client.getInputStream()));
        String input;

        System.out.println("Ожидаем сообщений");
        while ((input = in.readLine()) != null) {
            for(int i =0; i < _clients.size(); i++)
            {
                if(!_client.equals(_clients.get(i)))
                {
                    out = new PrintWriter(_clients.get(i).getOutputStream(),true);
                    out.println(input);
                }

            }
            System.out.println(input);
        }
    }

}