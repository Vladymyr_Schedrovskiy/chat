package controllers;

import javafx.application.Application;
import javafx.application.Platform;
import javafx.beans.property.ListProperty;
import javafx.beans.property.SimpleListProperty;
import javafx.collections.FXCollections;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.geometry.NodeOrientation;
import javafx.geometry.Orientation;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.VBox;
import javafx.scene.text.TextAlignment;
import model.*;
import network.Client;
import network.IMessageReceiverListener;

import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;

public class Controller implements IMessageReceiverListener, Initializable {
    @FXML
    private ListView<AnchorPane> chatBox;

    private ListProperty<AnchorPane> listProperty = new SimpleListProperty<>();
    private ArrayList<AnchorPane> messageBoxes = new ArrayList<>();
    @FXML
    private TextField messageText;
    private Client _client;
    private User _user;
    public Controller()  {
        try
        {
            Client client = new Client("localhost");
            client.addListener(this);
            _client = client;
            _client.startListenAsync();
        }
        catch (Exception ignored) {
            var st = ignored.getStackTrace();
        }
    }

    @FXML
    public void sendMessageClick() throws ParserConfigurationException {
        var message = messageText.getText();
        if(message.equals(""))
            return;

        addOwnMessage(message);
        messageText.clear();
        Message m = new Message(_user, message);
        _client.sendMessage(m);
    }

    public void addOwnMessage(String message)
    {
        AnchorPane messagePanel = new AnchorPane();
        Label label = new Label(message);
        label.setWrapText(true);
        label.setTextAlignment(TextAlignment.JUSTIFY);
        AnchorPane.setRightAnchor(label, 0d);
        messagePanel.getChildren().add(label);
        listProperty.add(messagePanel);
    }

    public void addExternalMessage(Message message)
    {
        var messageText = String.format("%s >>> %s", message.getSender().getName(), message.getContent());
        AnchorPane messagePanel = new AnchorPane();
        Label label = new Label(messageText);
        AnchorPane.setLeftAnchor(label, 0d);
        messagePanel.getChildren().add(label);
        Platform.runLater(() -> listProperty.add(messagePanel));
    }

    @Override
    public void MessageReceived(Message message) {
        addExternalMessage(message);
    }

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        chatBox.itemsProperty().bind(listProperty);
        listProperty.set(FXCollections.observableArrayList(messageBoxes));
    }

    public void setUser(User user) {
        this._user = user;
    }

    public User getUser() {
        return _user;
    }
}
