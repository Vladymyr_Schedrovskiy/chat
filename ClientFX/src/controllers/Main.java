package controllers;

import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.event.EventType;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.TextField;
import javafx.scene.layout.HBox;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;
import model.User;

public class Main extends Application {

    @Override
    public void start(Stage primaryStage) throws Exception{
        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("Main.fxml"));
        Parent root = fxmlLoader.load();
        var mainController = (Controller)fxmlLoader.getController();

        var x1= root.getUserData();

        var mainScene = new Scene(root, 300, 275);
        primaryStage.setMinHeight(275);
        primaryStage.setMinWidth(300);
        var x2 = mainScene.getUserData();
        Stage loginStage = createLoginScene(primaryStage, mainController);
        primaryStage.setScene(mainScene);
        primaryStage.show();
        loginStage.show();
        loginStage.setOnCloseRequest(event->{
            if(mainController.getUser() == null)
            {
                primaryStage.close();

            }
        });

    }

    private static Stage createLoginScene(Stage primaryStage, Controller mainController){
        TextField nicknameTextField = new TextField();
        nicknameTextField.setPromptText("Nickname");

        Button loginButton = new Button();
        loginButton.requestFocus();
        HBox hBox = new HBox(2, nicknameTextField, loginButton);

        Stage loginStage = new Stage();
        loginStage.setTitle("Enter your nickname");
        loginStage.setResizable(false);
        loginStage.initModality(Modality.APPLICATION_MODAL);
        loginStage.initOwner(primaryStage);
        loginStage.toFront();
        Scene loginScene = new Scene(hBox);
        loginStage.setScene(loginScene);
        loginButton.setText("Ok");
        loginButton.setOnAction(new EventHandler<ActionEvent>() {

            @Override
            public void handle(ActionEvent event) {
                if(nicknameTextField.getText().trim().isEmpty())
                    return;

                User user = new User();
                user.setName(nicknameTextField.getText().trim());
                loginStage.close();
                mainController.setUser(user);
                primaryStage.setTitle("Welcome " + mainController.getUser().getName());
            }
        });


        return loginStage;
    }





    public static void main(String[] args) {
        launch(args);
    }
}
