package model;

import org.w3c.dom.Document;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.IOException;
import java.io.StringReader;
import java.io.StringWriter;
import java.util.Date;

public class Message {
    private User _sender;
    private Date _timeAndZone;
    private String _content;

    public Message(User _sender, String _content) {
        this._sender = _sender;
        this._content = _content;
        _sender.addMessage(this);
        _timeAndZone = new Date();
    }

    public User getSender()
    {
        return _sender;
    }

    public Date getTimeAndZone(){
        return _timeAndZone;
    }

    public String getContent()
    {
        return _content;
    }

    public static Message FromJson(String source) throws ParserConfigurationException, IOException, SAXException {
        DocumentBuilder documentBuilder = DocumentBuilderFactory.newInstance().newDocumentBuilder();

        Document document = documentBuilder.parse(new InputSource(new StringReader(source)));
        var root = document.getDocumentElement();
        var xUser = root.getElementsByTagName("User").item(0);
        User user = new User();
        user.setName(xUser.getAttributes().getNamedItem("Name").getNodeValue());
        var xContent = document.getElementsByTagName("Content").item(0);
        Message message = new Message(user, xContent.getTextContent());
        return message;
    }

    public String ToJson() throws ParserConfigurationException {
        DocumentBuilder documentBuilder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
        Document document = documentBuilder.newDocument();
        var root = document.createElement("Message");
        document.appendChild(root);

        var xUser = document.createElement("User");
        var xUserName = document.createAttribute("Name");
        xUserName.setValue(_sender.getName());
        xUser.setAttributeNode(xUserName);
        root.appendChild(xUser);

        var xContent = document.createElement("Content");
        xContent.appendChild(document.createTextNode(_content));
        root.appendChild(xContent);

        var result = convertDocumentToString(document);
        return result;
    }

    private static String convertDocumentToString(Document doc) {
        TransformerFactory tf = TransformerFactory.newInstance();
        Transformer transformer;
        try {
            transformer = tf.newTransformer();
            // below code to remove XML declaration
            // transformer.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "yes");
            StringWriter writer = new StringWriter();
            transformer.transform(new DOMSource(doc), new StreamResult(writer));
            String output = writer.getBuffer().toString();
            return output;
        } catch (TransformerException e) {
            e.printStackTrace();
        }

        return null;
    }
}