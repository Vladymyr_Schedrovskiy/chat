package model;

import java.util.ArrayList;

public class User {
    private String _name;
    private ArrayList<Message> _sentMessages = new ArrayList<Message>();

    public String getName(){
        return _name;
    }

    public void setName(String name){
        _name = name;
    }

    public ArrayList<Message> getSentMessages(){
        return _sentMessages;
    }

    public void addMessage(Message message){
        _sentMessages.add(message);
    }



}
