package network;

import model.Message;
import org.xml.sax.SAXException;

import javax.xml.parsers.ParserConfigurationException;
import java.io.*;
import java.net.Socket;
import java.util.ArrayList;

public class Client  implements Runnable, Closeable{
    Socket server;
    private String _serverAddress;
    private PrintWriter _serverOutput;
    private BufferedReader _serverInput;
    private boolean _stoped = false;

    public Client(String serverAddress) throws IOException {
        if(serverAddress == null || serverAddress.isEmpty())
            throw new IllegalArgumentException("The server address is invalid");
        this._serverAddress = serverAddress;
    }

    private void start() throws IOException, ParserConfigurationException, SAXException {
        server  = new Socket(_serverAddress, 1234);

        _serverOutput =
                new PrintWriter(server.getOutputStream(),true);
        _serverInput = new BufferedReader(new InputStreamReader(server.getInputStream()));

        while (!_stoped)
        {
            var serverMessage = _serverInput.readLine();
            var message = Message.FromJson(serverMessage);
            EventInvoke(message);
        }
    }

    public void startListenAsync()
    {
        Thread listenerThread = new Thread(this);
        listenerThread.start();
    }

    public void sendMessage(Message message) throws ParserConfigurationException {
        if(!_stoped)
        {
            _serverOutput.println(message.ToJson());
        }
    }

    public void stop(){
        _stoped = true;
        close();
    }

    @Override
    public void close() {
        try {

            _serverInput.close();
            _serverOutput.close();
            server.close();
        }
        catch (Exception ignored){

        }
    }

    ArrayList<IMessageReceiverListener> listeners = new ArrayList<IMessageReceiverListener>();

    public void addListener(IMessageReceiverListener listener){
        listeners.add(listener);
    }

    public void removeListener(IMessageReceiverListener listener)
    {
        listeners.remove(listener);
    }

    private void EventInvoke(Message message)
    {
        for (IMessageReceiverListener listener:listeners) {
            listener.MessageReceived(message);
        }
    }

    @Override
    public void run() {
        try {
            start();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ParserConfigurationException e) {
            e.printStackTrace();
        } catch (SAXException e) {
            e.printStackTrace();
        }
    }
}
