package network;

import model.Message;

public interface IMessageReceiverListener{
    void MessageReceived(Message message);
}
